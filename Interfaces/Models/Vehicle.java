package Interfaces.Models;



/**
 * Created by root on 19.10.14.
 */
class OOPForKids {

    public static void main(String[] args) {
        Машина запор = new Запорожец("хреново");
        Машина субару = new Субару("отлично");
        запор.ехать();
        субару.ехать();
        ((ВедроСБолтами) запор).тормозить();
        ((ГоночнаяТачка) субару).крутоеМузло();
    }
}

interface Машина {

    void ехать();
}

abstract class ВедроСБолтами {

    private String name = this.getClass().getName();

    public String getName() {
        return name;
    }
    private String качество;

    public void setКачество(String качество) {
        this.качество = качество;
    }

    void налево() {
        System.out.println(name + " налево " + качество);
    }

    void направо() {
        System.out.println(name + " направо " + качество);
    }

    void тормозить() {
        System.out.println(name + " тормозим " + качество);
    }
}

abstract class ГоночнаяТачка {

    private String name = this.getClass().getName();

    public String getName() {
        return name;
    }
    private String качество;

    public void setКачество(String качество) {
        this.качество = качество;
    }

    void налево() {
        System.out.println(name + " налево в заносе " + качество);
    }

    void направо() {
        System.out.println(name + " направо в заносе" + качество);
    }

    void тормозить() {
        System.out.println(name + " тормозим с дымом " + качество);
    }

    abstract void азот();

    abstract void гидравлика();

    abstract void крутоеМузло();
}

class Запорожец extends ВедроСБолтами implements Машина {

    private String качество = "хреново";

    Запорожец(String string) {
        this.качество = string;
    }

    @Override
    void налево() {
        System.out.println(super.getName() + " налево " + качество);
    }

    @Override
    void направо() {
        System.out.println(super.getName() + " направо " + качество);
    }

    @Override
    void тормозить() {
        System.out.println(super.getName() + " тормозим " + качество);
    }

    @Override
    public void ехать() {
        System.out.println(super.getName() + " едем " + качество);
    }
}

class Субару extends ГоночнаяТачка implements Машина {

    private String качество = "отлично";
    private String name = this.getClass().getName();

    Субару(String string) {
        this.качество = string;
    }

    @Override
    public void ехать() {
        System.out.println(super.getName() + " едем " + качество);
    }

    @Override
    void азот() {
        System.out.println(super.getName() + " пыщ пыщ " + качество);
    }

    @Override
    void гидравлика() {
        System.out.println(super.getName() + " прыг скок " + качество);
    }

    @Override
    void крутоеМузло() {
        System.out.println(super.getName() + " ё ё браза браза " + качество);
    }
}