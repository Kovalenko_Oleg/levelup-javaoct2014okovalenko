package com.sz.files;
import java.io.*;
import java.nio.*;
import java.nio.file.Files;

/**
 * Created by root on 16.11.14.
 */
public class OnlyExt implements FilenameFilter {
   String ext;

    public OnlyExt(String ext) {
        this.ext = ext;
    }

    public boolean accept(File dir, String name){
       return name.startsWith(ext);
   }
}

class DirDirect {

    final String pathFrom = "/home/Test/";
    final String pathTo = "/home/Test1/";
    final String fileName = "image";

    void dir() {
        File pFrom = new File(pathFrom);
        File pTo = new File(pathTo);
        FilenameFilter Filter = new OnlyExt(fileName);
        File[] From = pFrom.listFiles(Filter);
        File[] To = pTo.listFiles(Filter);


        System.out.println("Copy files from:  ");
        for (int i = 0; i < From.length; i++) {
            System.out.println(From[i]);
        }



        System.out.println("\nTo:  ");
        for (int i = 0; i < To.length; i++) {
            System.out.println(To[i]);
        }
      }
}

class DirListOnly{
    public static void main(String[] args) {
        DirDirect dir = new DirDirect();
        dir.dir();

    }
}




