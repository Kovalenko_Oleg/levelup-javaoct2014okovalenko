/**
 Задание от 07.10.2014
 Создать свою иерархию классов. Во главе иерархии будет абстрактный класс с парой-тройкой полей, геттеры сеттеры, конструктор с параметрами,
 а также пара абстрактных методов, которые будут реализованы в потомках. Также должен присутствовать переопределенный метод toString().
 Создать два-три потомка. Каждый должен содержать пару дополнительных полей. Коструктор с параметрами должен использовать конструктор предка, также как и метод toString().
 Для примера можно использовать иерархию Pet(Домашнее животное), Cat, Dog, Bird
 */

package LW_1;

/**
 * Created by jerald on 08.10.14.
 */
abstract class Vihicle {
    int numberOfWheel;
    int numberOfDoors;
    double engine;
    private String type = "Транспортное средство";

    public void setType(String type){
        this.type = type;
    }

    @Override
    public String toString(){
        return "Тип: " + type;
    };

    Vihicle(int numberOfWheel,int numberOfDoors, double engine){
        this.numberOfWheel = numberOfWheel;
        this.numberOfDoors = numberOfDoors;
        this.engine = engine;
    }

    abstract void Action();

}

class Car extends Vihicle{

    String view = "Машина";
    String marka = "Toyota";

    Car(int numberOfWheels, int numberOfDoors, double engine){
        super(numberOfWheels, numberOfDoors, engine);
    }

    @Override
    public String toString()  {
        return super.toString() + "\nВид: " + view + "\nМарка: " + marka;
    }

    public void vihParam(){
        System.out.println("\nКоличество колес: " + numberOfWheel + "\nКоличество дверей: " + numberOfDoors + "\nОбьем движка: " + engine);
    }

    public void Action(){
        System.out.println("Едет");
    }

}

class Plane extends Vihicle{
    String view = "Самолёт";
    String marka = "BOING";
    int numberOfKr;


    Plane(int numberOfWheel, int numberOfDoors, double engine, int numberOfKr) {
        super(numberOfWheel, numberOfDoors, engine);
        this.numberOfKr = numberOfKr;
    }

    @Override
    public String toString(){
        return super.toString() + "\nВид: " + view + "\nМарка: " + marka;
    }

    public void plParam(){
        System.out.println("\nКоличество колес: " + numberOfWheel + "\nКоличество дверей: " + numberOfDoors +
                "\nОбьем движка: " + engine + "\nКоличество крыльев: " + numberOfKr);
    }

    void Action(){
        System.out.println("Летит");
    }

}


class VihicleOut{
    public static void main(String[] args) {
        Car c = new Car(4,4,3.6);
        Plane p = new Plane(3,1,20,5);
        System.out.println(c.toString());
        c.vihParam();
        c.Action();
        System.out.println("\n---------------------------------\n" + p.toString());
        p.plParam();
        p.Action();

        /*    Car c = new Car(1);
    Vihicle v;

        System.out.println(c.toString());*/


    }
}